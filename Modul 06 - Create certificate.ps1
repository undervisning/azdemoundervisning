# Oppretter sertifikat for sikker tilkobling
$certStoreLocation = "Cert:\CurrentUser\My"
Get-ChildItem -Path "Cert:\CurrentUser\My"

# Oppretter self-signed root certificate
$cert = New-SelfSignedCertificate -Type Custom -KeySpec Signature `
                                    -Subject "CN=P2SRootCert" -KeyExportPolicy Exportable `
                                    -HashAlgorithm sha256 -KeyLength 2048 `
                                    -CertStoreLocation $certStoreLocation -KeyUsageProperty Sign -KeyUsage CertSign

New-SelfSignedCertificate -Type Custom -DnsName "P2SRootClient" -KeySpec Signature `
                            -Subject "CN=P2SRootClient" -KeyExportPolicy Exportable `
                            -HashAlgorithm sha256 -KeyLength 2048 `
                            -CertStoreLocation $certStoreLocation `
                            -Signer $cert -TextExtension @("2.5.29.37={text}1.3.6.1.5.5.7.3.2")
