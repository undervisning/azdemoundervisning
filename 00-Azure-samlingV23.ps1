# Mulig pre req:
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco upgrade chocolatey
# Installere programvare med Choco
choco install -y powershell-core
choco install -y git.install
choco install -y vscode


Install-Module -Name Az
#Get-Help Get-AzSubscription -online

# Merk at om en ikke spesifisere Tenant ID ved tilkobling må en spesifisere hvilken tenant en ønsker å jobbe med
Get-AzTenant
Get-AzTenant | Where-object {$_.name -like "SKRIV NAVN" }
$tenants = Get-AzTenant | Where-object {$_.name -like "Demo Undervisning" }
$tenants
Set-AzContext -TenantId $tenants.Id
Get-AzContext
$Subscription = Get-AzSubscription -SubscriptionName "Azure for Students"
#get-member?

################################################################################################
# Variabler brukt for oppretting av ResourceGroup VNET og subnett i scriptet

# For alle:
$initials = "tim"
$location = "westeurope"
$rgvnet = "rg-vnet-$location-$initials"

# VNET 1
$vnet1 = "vnet001"
$vnet1name = "vnet-$initials-$location-001"
$vnet1AddressPrefix = "10.10.0.0/19"
$vnet1subnet1name = "snet-$vnet1-$location-001"
$vnet1subnet1AdressPrefix = "10.10.1.0/24"

# VNET 2
$vnet2 = "vnet002"
$vnet2name = "vnet-$initials-$location-002"
$vnet2AddressPrefix = "10.20.0.0/19"
$vnet2subnet1name = "snet-$vnet2-$location-001"
$vnet2subnet1AdressPrefix = "10.20.1.0/24"

################################################################################################

# Oppretter RG med navnet i variablen $rgvnet og legger den på lokasjonen spesifisert i $location
$newrg = @{
    Name = $rgvnet
    Location = $location
}
New-AzResourceGroup @newrg

################################################################################################

# Oppretter VNET1 med oppgitt størrelse og subnett med oppgitt størrelse
$newvnet = @{
    Name = $vnet1name
    AddressPrefix = $vnet1AddressPrefix
    ResourceGroupName = $rgvnet
    Location = $location
}
# Vi legger det nyopprettede VNET-et i en variabel, siden vi må oppdatere det med subnett-informasjo
# i kommandoen under for opprettelsen av suvnettet
$vnet1info = New-AzVirtualNetwork @newvnet

# Oppretter snet1 i vnet1
$vnet1subnet1 = @{
    Name = $vnet1subnet1name
    AddressPrefix = $vnet1subnet1AdressPrefix
    VirtualNetwork = $vnet1info
}
$vnet1subnet1info = Add-AzVirtualNetworkSubnetConfig @vnet1subnet1
# Add-AzVirtualNetworkSubnetConfig 
# https://docs.microsoft.com/en-us/powershell/module/az.network/add-azvirtualnetworksubnetconfig?view=azps-7.3.2
# Legger til nylig opprettet subnet i VNET1
$vnet1info = $vnet1info | Set-AzVirtualNetwork

# Oppretter VNET2 med oppgitt størrelse og subnett med oppgitt størrelse uten kommentarer i koden (likt som over)
# VNET2
$newvnet = @{
    Name = $vnet2name
    AddressPrefix = $vnet2AddressPrefix
    ResourceGroupName = $rgvnet
    Location = $location
}
$vnet2info = New-AzVirtualNetwork @newvnet
# Subnet 1 og 2 for VNET2
$vnet2subnet1 = @{
    Name = $vnet2subnet1name
    AddressPrefix = $vnet2subnet1AdressPrefix
    VirtualNetwork = $vnet2info
}
$vnet2subnet1info = Add-AzVirtualNetworkSubnetConfig @vnet2subnet1
# Legger til Subnet
$vnet2info = $vnet2info | Set-AzVirtualNetwork

<# For å Fjerne Subnett: Merk at -VirtualNetwork må inneholder informasjon til VNET
F.eks: 
$virtualNetwork = Get-AzVirtualNetwork -Name <navnpåsubnet>
Remove-AzVirtualNetworkSubnetConfig -Name <navnpåsubnet> -VirtualNetwork $virtualNetwork
$virtualNetwork | Set-AzVirtualNetwork
#>

<#
  10.10.0.0/19        10.20.0.0/19
_________________   _________________
|    VNET 1     |   |    VNET 2     |
|               |   |               |
| SNET1         |   | SNET1         |
|               |   |               |
|_______________|   |_______________|
  10.10.1.0/24        10.20.1.0/24

#>

################################################################################################
# VM-er
$vmv1s1 = "vm-$initials-v1s1"
$vmv2s1 = "vm-$initials-v2s1"

$VNETVMs = @(
  @{
    VmName = $vmv1s1
    nicName = "$vmv1s1-nic"
    ip = "10.10.1.4"
    vmsize = "Standard_D2s_v3"
    vNet = $vnet1name
    subnet = $vnet1subnet1name
  },
  @{
    VmName = $vmv2s1
    nicName = "$vmv2s1-nic"
    ip = "10.20.1.4"
    vmsize = "Standard_D2s_v3"
    vNet = $vnet2name
    subnet = $vnet2subnet1name
  }
)

# Påloggingsinformasjon til VM
$cred = Get-Credential

foreach($vm in $VNETVMs){
 
    $vnetinfo = Get-AzVirtualNetwork -name $vm.vNet
    $subnetinfo = Get-AzVirtualNetworkSubnetConfig -Name $vm.subnet -VirtualNetwork $vnetinfo
    $IPconfig = New-AzNetworkInterfaceIpConfig -Name "IPConfig1" -PrivateIpAddressVersion IPv4 -PrivateIpAddress $vm.ip -SubnetId $subnetinfo.id
    $NIC = New-AzNetworkInterface -Name $vm.nicName -ResourceGroupName $rgvnet -Location $location -IpConfiguration $IPconfig
     
    $VirtualMachine = New-AzVMConfig -VMName $vm.VmName -VMSize $vm.vmsize
    $VirtualMachine = Set-AzVMOperatingSystem -VM $VirtualMachine -Windows -ComputerName $vm.VmName -Credential $cred -ProvisionVMAgent -EnableAutoUpdate
    $VirtualMachine = Add-AzVMNetworkInterface -VM $VirtualMachine -Id $NIC.Id
    $VirtualMachine = Set-AzVMSourceImage -VM $VirtualMachine -PublisherName 'MicrosoftWindowsServer' -Offer 'WindowsServer' -Skus '2019-Datacenter' -Version latest
    
    # New-AzVM - https://docs.microsoft.com/en-us/powershell/module/az.compute/new-azvm?view=azps-7.3.2
    New-AzVM -ResourceGroupName $rgvnet -Location $location -VM $VirtualMachine -Verbose -AsJob
}

# Legger til en brannmurregel så maskinene svarer på PING.
# Invoke-AzVmRunCommand - https://docs.microsoft.com/en-us/powershell/module/az.compute/invoke-azvmruncommand?view=azps-7.3.2
#  
$vmnames = @($vmv1s1, $vmv2s1)
$vmnames | ForEach-Object {
    "VM: " + "$_"
Invoke-AzVmRunCommand `
     -ResourceGroupName $rgvnet `
     -VMName $_ `
     -CommandId "RunPowerShellScript" `
     -ScriptPath "azdemoundervisning/azinvoke-firewall.ps1" -AsJob
    }


# Test ping
# Ping mellom VM-er på forskjellige SUBNET i samme VNET fungerer
# Ping mellom VM-er på forskjellige SUBNET i forksjellige VNET fungerer ikke

# PEERING muliggjør kommunikasjon på kryss av VNET
$vnet1vnet2peeringname = "vnet1-vnet2-peering-$initials"
$vnet2vnet1peeringname = "vnet2-vnet1-peering-$initials"
# Henter inn nødvendig informasjon fra VNET og legger det i variabler:
$vnet1info = Get-AzVirtualNetwork -Name $vnet1name
$vnet2info = Get-AzVirtualNetwork -Name $vnet2name

$VNET1toVNET2 = @{
    Name = $vnet1vnet2peeringname
    VirtualNetwork = $vnet1info
    RemoteVirtualNetworkId = $vnet2info.Id
}
Add-AzVirtualNetworkPeering @VNET1toVNET2

$VNET2toVNET1 = @{
    Name = $vnet2vnet1peeringname
    VirtualNetwork = $vnet2info
    RemoteVirtualNetworkId = $vnet1info.Id
}
Add-AzVirtualNetworkPeering @VNET2toVNET1

# Test ping i Portal
# Ping mellom VM-er på forskjellige SUBNET i samme VNET fungerer
# Ping mellom VM-er på forskjellige SUBNET i forksjellige VNET fungerer

$vmnames = @($vmv1s1)
$vmnames | ForEach-Object {
    "VM: " + "$_ forsøker å pinge den andre VM-en"
Invoke-AzVmRunCommand `
     -ResourceGroupName $rgvnet `
     -VMName $_ `
     -CommandId "RunPowerShellScript" `
     -ScriptPath "azdemoundervisning/azinvoke-ping.ps1"
    }

