resourceGroupName="rg-vm-backup-demo-002" # Change this to your preferred resource group name
location="uksouth" # Change this to your preferred Azure location
vmName="vm-demo-002" # Change this to your preferred VM name
vaultName="vault-vm-backup-demo-002" # Change this to your preferred backup vault name
redundancy="LocallyRedundant" # Locally redundant storage - Allowed values: GeoRedundant, LocallyRedundant
backupManagementType="AzureIaasVM" # Azure IaaS VM backup - allowd values: AzureWorkload, AzureIaasVM, MAB, DPM, AzureStorage
workloadType="VM" # VM backup - allowed values: VM, FileFolder, SQLDataBase, SQLInstance, SAPASE, SAPHana, SystemState, Client, Exchange, SharePoint, DataShare, AzureFileShare, AzureWorkload, GenericData, MSSQLDataBase, MSSQLInstance, MySQL, PostgreSQL, MariaDB, Oracle, AzureSQLDatabase, AzureSQLInstance, AzureFileShare, AzureWorkload
policyName="EnhancedPolicy" # Change this to your created backup policy name, accepted default values: DefaultPolicy, EnhancedPolicy, GrandfatherPolicy, InstantRP, RPOPolicy, SimpleBackupPolicy, TransactionLogBackupPolicy
storageAccountName="stvmbackupdemo002" # Change this to your preferred storage account name

az backup vault create \
    --resource-group $resourceGroupName \
    --name $vaultName \
    --location $location

# Backup Vault Properties
az backup vault backup-properties set \
    --name $vaultName \
    --resource-group $resourceGroupName \
    --backup-storage-redundancy $redundancy

# Enable backup for the virtual machine
az backup protection enable-for-vm \
    --resource-group $resourceGroupName \
    --vault-name $vaultName \
    --vm $vmName \
    --policy-name $policyName

# Start a backup job
az backup protection backup-now \
    --resource-group $resourceGroupName \
    --vault-name $vaultName \
    --container-name $vmName \
    --item-name $vmName \
    --backup-management-type $backupManagementType \
    --workload-type $workloadType

# Monitor the backup job
az backup job list \
    --resource-group $resourceGroupName \
    --vault-name $vaultName \
    --output table

# Create storage account for restore
az storage account create --name $storageAccountName  \
    --resource-group $resourceGroupName \
    --location $location \
    --sku Standard_LRS \
    --kind StorageV2