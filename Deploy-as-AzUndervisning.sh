group=rg-demo-vm-001
az group create -g $group -l uksouth
username=azundervisning
password='mitThemmeliGePass0r123!'

az network vnet create \
  -n vnet-lb-demo-uksouth \
  -g $group \
  -l uksouth \
  --address-prefixes '10.0.0.0/16' \
  --subnet-name snet-lb-demo-uksouth \
  --subnet-prefixes '10.0.1.0/24'

  
az vm availability-set create \
  -n vm-as-demo-uksouth \
  -l uksouth \
  -g $group

for NUM in 1 2 3
do
  az vm create \
    -n vm-demo-we-0$NUM \
    -g $group \
    -l uksouth \
    --size Standard_B1ms \
    --image Win2019Datacenter \
    --admin-username $username \
    --admin-password $password \
    --vnet-name vnet-demo-uksouth-001 \
    --subnet snet-demo-uksouth-001 \
    --public-ip-address "" \
    --availability-set vm-as-demo-uksouth \
	  --nsg nsg-vm-as-demo
done

for NUM in 1 2 3
do
  az vm open-port -g $group --name vm-demo-we-0$NUM --port 80
done

for NUM in 1 2 3
do
  az vm extension set \
    --name CustomScriptExtension \
    --vm-name vm-demo-we-0$NUM \
    -g $group \
    --publisher Microsoft.Compute \
    --version 1.8 \
    --settings '{"commandToExecute":"powershell Add-WindowsFeature Web-Server; powershell Add-Content -Path \"C:\\inetpub\\wwwroot\\Default.htm\" -Value $($env:computername)"}'
done
