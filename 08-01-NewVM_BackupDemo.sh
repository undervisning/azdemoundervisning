# Variables
resourceGroupName="rg-vm-backup-demo-002" # Change this to your preferred resource group name
location="uksouth" # Change this to your preferred Azure location
vmName="vm-demo-002" # Change this to your preferred VM name
vmSize="Standard_B1s" # VM size, B1s is small and cost-effective
image="Ubuntu2204" # Operating system image. Ubuntu LTS is a good choice for a wide range of applications
username="azundervisning" # Change this to your preferred username
password="mitThemmeliGePass0r123!" # Change this to your preferred password
vnetname="vnet-demo-002" # Change this to your preferred virtual network name
snetname="snet-lb-demo-002" # Change this to your preferred subnet name
nsgname="nsg-vm-demo" # Change this to your preferred network security group name

# Create a resource group
az group create --name $resourceGroupName --location $location

# Create a virtual machine
az network vnet create \
  -n $vnetname \
  -g $resourceGroupName \
  -l $location \
  --address-prefixes '10.1.0.0/16' \
  --subnet-name $snetname \
  --subnet-prefixes '10.1.1.0/24'

az vm create \
  -n $vmName \
  -g $resourceGroupName \
  -l $location \
  --size $vmSize \
  --image $image \
  --admin-username $username \
  --admin-password $password \
  --vnet-name $vnetname \
  --subnet $snetname \
  --public-ip-address "" \
  --nsg $nsgname