$group = "rg-demo-we"
$storageAccountName = "satimdemo001"
$account = Get-AzStorageAccount -ResourceGroupName $group -Name $storageAccountName
$ctx = $account.Context
$table = "pcutleie"

$cloudtable = (Get-AzStorageTable -Name $table -Context $ctx).CloudTable

Get-AzTableRow -table $cloudTable | ft

$partitionKeyPC = "pc"
$partitionKeySkjerm = "skjerm"



$customFilter = "(PartitionKey eq 'pc') and (RowKey eq '1234567')"
Get-AzTableRow -table $cloudTable -customFilter $customFilter
$pc1 = Get-AzTableRow -table $cloudTable -customFilter $customFilter

$pc1.epost = "frank.jensen@tisipdemostudent.onmicrosoft.com"
$pc1.givenName = "Frank"
$pc1.surname = "Jensen"
$pc1.tlf = "40808080"

$pc1 | Update-AzTableRow -table $cloudTable




# Create a filter and get the entity to be updated.
[string]$filter = [Microsoft.Azure.Cosmos.Table.TableQuery]::GenerateFilterCondition("username", [Microsoft.Azure.Cosmos.Table.QueryComparisons]::Equal,"Jessie")
$pc = Get-AzTableRow -table $cloudTable -customFilter $filter

# Change the entity.
$user.username = "Jessie2"

# To commit the change, pipe the updated record into the update cmdlet.
$user | Update-AzTableRow -table $cloudTable

# To see the new record, query the table.
$customFilter = "(PartitionKey eq 'pc') and (RowKey eq '1234567')"
Get-AzTableRow -table $cloudTable -customFilter $customFilter




# Create new table
$tablename = "utleiedemo"
New-AzStorageTable -Name $tablename -Context $ctx

