#!/bin/bash

# Define variables
resourceGroupName="rg-demo-we"
location="westeurope"
storageAccountName="satimdemo001"

# Create a Resource Group
echo "Creating Resource Group: $resourceGroupName in $location."
az group create --name $resourceGroupName --location $location

# Validate the creation of the Resource Group
# This step is optional but recommended to ensure that the Resource Group was created successfully
echo "Validating the creation of Resource Group: $resourceGroupName."
az group show --name $resourceGroupName

# Create a Storage Account within the created Resource Group
echo "Creating Storage Account: $storageAccountName in Resource Group: $resourceGroupName."
az storage account create --name $storageAccountName --resource-group $resourceGroupName --location $location --sku Standard_LRS --kind StorageV2

# Validate the creation of the Storage Account
# This step is also optional but recommended to ensure that the Storage Account was created successfully
echo "Validating the creation of Storage Account: $storageAccountName."
az storage account show --name $storageAccountName --resource-group $resourceGroupName
