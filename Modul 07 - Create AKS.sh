
az group create --name rg-aks-demo --location westeurope

git clone https://github.com/Azure-Samples/azure-voting-app-redis.git
az acr create --resource-group rg-aks-demo --name criaksdemo --sku Basic
az acr build --image demo/votinappp:v1 --registry criaksdemo --file Dockerfile .

az aks create \
    --resource-group rg-aks-demo \
    --name aksdemocluster \
    --node-count 2 \
    --generate-ssh-keys \
    --attach-acr criaksdemo

# az aks get-credentials --resource-group rg-aks-demo --name aksdemocluster
# az acr list --resource-group rg-aks-demo --query "[].{acrLoginServer:loginServer}" --output table
# kubectl get nodes

kubectl apply -f azure-vote-all-in-one-redis.yaml
kubectl get service azure-vote-front --watch

kubectl get pods
kubectl scale --replicas=5 deployment/azure-vote-front

kubectl autoscale deployment azure-vote-front --cpu-percent=50 --min=3 --max=10


# Update App image
kubectl set image deployment azure-vote-front azure-vote-front=criaksdemo.azurecr.io/demo/votinappp:v2
